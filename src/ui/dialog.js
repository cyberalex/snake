import { Button } from "./button";
import Control from "./control";

export class Dialog extends Control {
    constructor(templateId) {
        const dialog = document.querySelector(`#${templateId}`).content.cloneNode(true);
        document.body.appendChild(dialog);
        super('.dialog');
    }

    fade() {
        this.view.querySelector('.dialog__content-inner').classList.remove('scale1');
        return new Promise((resolveTransition) => {
            this.view.querySelector('.dialog__content-inner').addEventListener('transitionend', () => {
                resolveTransition();
            }, { once: true });
        }).then(() => this.hide());
    }

    async close(resolveFunc, result) {
        await this.fade();
        document.removeEventListener('keydown', this.dialogKeyPress, false);
        this.view.remove();
        resolveFunc(result);
    }
    
    showView() {
        super.show();
        setTimeout(() => this.view.querySelector('.dialog__content-inner').classList.add('scale1'));
    }
}

export class InfoDialog extends Dialog {
    closeButton;

    constructor(templateId) {
        super(templateId);
        this.closeButton = new Button(this.view.querySelector('.button_close'));
    }

    show() {
        this.showView();
        this.closeButton.view.focus();
        return new Promise((resolve) => {
            const dialogKeyPress = (event) => {
                if (event.code === 'Escape') {
                    this.close(resolve);
                }
            }
            document.addEventListener('keydown', dialogKeyPress);
            this.closeButton.setOnClick(() => this.close(resolve));
        });
    }
}
