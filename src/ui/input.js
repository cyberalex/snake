import Control from "./control";

export default class Input extends Control {
    constructor(element, value = '') {
        super(element);
        this.value = value;
    }

    get value() {
        return this.view.value;
    }

    set value(v) {
        this.view.value = v;
    }
}