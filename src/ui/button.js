import Control from "./control";

export class Button extends Control {
    constructor(element) {
        super(element);
    }

    setOnClick(callback) {
        this.view.addEventListener('click', callback);
    }
    
    setRound() {
        this.view.classList.add('button_round');
    }

    setSquare() {
        this.view.classList.remove('button_round');
    }

    get isRound() {
        return this.view.classList.contains('button_round');
    }
}

export class StartButton extends Button {
    textPause;
    textPlay;

    constructor(element) {
        super(element);
        this.textPause = new Control(this.view.querySelector('.button__text_pause'));
        this.textPlay = new Control(this.view.querySelector('.button__text_play'));
    }

    setStatePlay() {
        this.view.classList.add('button_start_play');
		this.view.classList.remove('button_start_pause');
        this.textPause.hide();
        this.textPlay.show();
    }

    setStatePause() {
        this.view.classList.add('button_start_pause');
		this.view.classList.remove('button_start_play');
        this.textPause.show();
        this.textPlay.hide();
    }

    setOnClick(callback) {
        this.view.addEventListener('click', (event) => {
            event.currentTarget.blur();
            callback();
        });
    }
}