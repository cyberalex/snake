import FieldCell from "./field-cell";

export default class FieldRow {
    view;
    cells = [];

    constructor(height) {
        this.view = document.createElement('div');
        this.view.classList.add('game__row');
        this.height = height;
    }

    addCell() {
        const cell = new FieldCell(this.height);
        this.view.append(cell.view);
        this.cells.push(cell);
    }

    fillCells(count) {
        for (let j = 0; j < count; j++) {
            this.addCell();
        }
    }

    get cellsCount() {
        return this.cells.length;
    }

    set height(value) {
        this.view.style.height = value;
    }

    get height() {
        return parseFloat(getComputedStyle(this.view).height);
    }
}