export default class FieldCell {
    view;

    constructor(size) {
        this.view = document.createElement('span');
        this.view.classList.add('game__cell');
        this.size = size;
    }

    set size(value) {
        this.view.style.height = value;
        this.view.style.width = value;
    }

    clear() {
        this.view.innerHTML = '';
        this.view.removeAttribute('class');
        this.view.classList.add('game__cell');
    }

    setReset(targetIndex) {
        this.view.classList.add(`game__cell_reset${targetIndex}`);
    }

    removeReset(targetIndex) {
        this.view.classList.remove(`game__cell_reset${targetIndex}`);
        this.showImage();
    }

    hideImage() {
        if (this.img) {
            this.img.style.display = 'none';
        }
    }

    showImage() {
        if (this.img) {
            this.img.style.display = 'block';
        }
    }

    get img() {
        return this.view.querySelector('img');
    }

    set img(value) {
        this.view.querySelector('img').src = value;
    }
}