import Brick from "./brick";

export default class BrickInRow extends Brick {
    constructor() {
        super();
        this.view.classList.add('game__brick_row');
    }
}