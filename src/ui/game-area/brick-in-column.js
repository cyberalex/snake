import Brick from "./brick";

export default class BrickInColumn extends Brick {
    constructor() {
        super();
        this.view.classList.add('game__brick_column');
    }
}