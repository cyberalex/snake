import FieldRow from "./field-row";

export default class Field {
    view;
    rows = [];
    resetTargetIndex;

    constructor() {
        this.view = document.querySelector('.game__field');
    }

    init(rowsCount, columnsCount, cellSize) {
        this.view.innerHTML = '';
        this.rows = [];
        for (let i = 0; i < rowsCount; i++) {
            const row = new FieldRow(cellSize);
            row.fillCells(columnsCount);
            this.view.append(row.view);
            this.rows.push(row);
        }
    }

    clear() {
        this.rows.forEach((row) => row.cells.forEach((cell) => cell.clear()));
    }

    async resetDownAnimation(resetSpeed, targetIndex) {
        this.resetTargetIndex = targetIndex;
        return new Promise(async (resolveReset) => {
            for (let row of this.rows) {
                row.cells.forEach((cell) => {
                    cell.clear();
                    cell.setReset(targetIndex);
                });
                await new Promise((resolve) => setTimeout(resolve, resetSpeed));
            }
            resolveReset();
        });
    }

    async resetUpAnimation(resetSpeed) {
        this.rows.forEach((row) => row.cells.forEach((cell) => {
            cell.setReset(this.resetTargetIndex);
            cell.hideImage();
        }));
        
        return new Promise(async (resolveReset) => {
            for (let row of this.rows.toReversed()) {
                row.cells.forEach((cell) => cell.removeReset(this.resetTargetIndex));
                await new Promise((resolve) => setTimeout(resolve, resetSpeed));
            }
            resolveReset();
        });
    }
    
    get rowsCount() {
        return this.rows.length;
    }

    get columnsCount() {
        return this.rows[0].cellsCount;
    }

    set cellSize(value) {
        this.rows.forEach((row) => {
            row.height = value;
            row.cells.forEach((cell) => cell.size = value);
        });
    }

    get cellSize() {
        return this.rows[0].height;
    }
}