import BrickInColumn from "./brick-in-column";
import BrickInRow from "./brick-in-row";

export default class Wall {
    rowsCount = 0;
    columnsCount = 0;
    #baseSize = 0;

    init(fieldRowsCount, filedColumnsCount, baseSize) {
        this.rowsCount = Math.floor(fieldRowsCount / 2);
        this.columnsCount = Math.floor(filedColumnsCount / 2);
        this.#baseSize = baseSize;
        this.fillBricks();
    }

    fillBricks() {
        document.querySelectorAll('.game__wall_row').forEach((wall) => {
            wall.style.height = this.#baseSize;
            wall.innerHTML = '';
            for (let i = 0; i < this.columnsCount; i++) {
                wall.append(new BrickInRow().view);
            }
        });
        document.querySelectorAll('.game__wall_column').forEach((wall) => {
            wall.style.width = this.#baseSize;
            wall.innerHTML = '';
            for (let i = 0; i < this.rowsCount; i++) {
                wall.append(new BrickInColumn().view);
            }
        });
    }

    set baseSize(value) {
        this.#baseSize = value;
        document.querySelectorAll('.game__wall_row').forEach((wall) => wall.style.height = value);
        document.querySelectorAll('.game__wall_column').forEach((wall) => wall.style.width = value);
    }
}