export default class Brick {
    view;

    constructor() {
        this.view = document.createElement('div');
        this.view.classList.add('game__brick');
    }
}