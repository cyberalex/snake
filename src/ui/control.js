export default class Control {
    view;

    constructor(element) {
        this.view = element instanceof HTMLElement ? element : document.querySelector(element);
    }

    hide() {
        this.view.classList.add('hide');
        this.view.style.display = null;
    }

    isVisible() {
        return !this.view.classList.contains('hide');
    }

    show(display) {
        this.view.classList.remove('hide');
        if (display) {
            this.view.style.display = display;
        }
    }

    disable() {
        this.view.disabled = true;
    }

    enable() {
        this.view.disabled = null;
    }
}