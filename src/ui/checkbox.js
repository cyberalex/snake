import Control from "./control";

export default class CheckBox extends Control {
    constructor(element, checked = false) {
        super(element);
        this.checked = checked;
    }

    get checked() {
        return this.view.checked;
    }

    set checked(v) {
        this.view.checked = v;
    }
}