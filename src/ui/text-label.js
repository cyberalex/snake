import Control from "./control";

export class TextLabel extends Control {
    constructor(element) {
        super(element);
    }

    set text(value) {
        this.view.textContent = value;
    }

    get text() {
        return this.view.textContent;
    }
}