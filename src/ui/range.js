import Control from "./control";

export default class Range extends Control {
    label;

    constructor(element, value, min, max) {
        super(element);
        this.label = new Control(this.view.parentElement.querySelector('.dialog__input-value'));
        this.value = value;
        this.view.min = min;
        this.view.max = max;

        this.view.addEventListener('input', () => {
            this.label.view.textContent = this.value;
        });
    }

    get value() {
        return this.view.value;
    }

    set value(v) {
        this.view.value = v;
        this.label.view.textContent = v;
    }
}