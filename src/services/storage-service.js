export function saveSettings(settings) {
    Object.keys(settings).forEach((key) => localStorage[key] = settings[key]);
}

export function loadSettings() {
    const settings = {};
    Object.keys(localStorage).forEach((key) => key !== 'length' ? settings[key] = localStorage[key] : null);
    if (settings.keypad) {
        settings.keypad = JSON.parse(settings.keypad);
    }
	return settings;
}