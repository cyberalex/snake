import { App } from "./modules/app";
import { appDate } from "./shared/app-info";
import "../scss/styles.scss";

document.querySelector('.app-date').textContent = appDate;
const app = new App();
