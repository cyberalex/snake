import AboutDialog from "../components/dialogs/about-dialog";
import ConfigDialog from "../components/dialogs/config-dialog";
import InputController from "./input-controller";
import Game from "./game";
import UsernameLabel from "../components/username-label";
import * as storage from "../services/storage-service";
import { isTouchDevice, vibrateOnClick } from "../shared/shared";
import { defaultCellSize, minCellSize } from "../shared/settings";

export class App {
    inputController;
    game;
    usernameLabel;
    view;

    constructor() {
        this.view = document.querySelector('.game');
        this.usernameLabel = new UsernameLabel(); 
        this.game = new Game();
        this.inputController = new InputController(
            this.game.newGame.bind(this.game),
            this.gameToggle.bind(this),
            this.showConfigDialog.bind(this),
            this.showAboutDialog.bind(this),
            this.processKey.bind(this)
        );
        window.addEventListener('resize', this.onResize.bind(this));
        window.addEventListener('gameStop', this.onGameStop.bind(this));
        window.addEventListener('gameStart', this.onGameStart.bind(this));
        document.addEventListener('keydown', this.keyPressed.bind(this));
        this.game.areaView.addEventListener('click', this.fieldClick.bind(this));
        this.loadSettings();
        this.setGameFieldSizes();
    }
    
    setGameFieldSizes() {
		const bottom = isTouchDevice() ?  this.inputController.keypad.clientHeight : document.querySelector('footer').clientHeight;
		this.game.maxRowsCount = Math.floor(((window.screen.availHeight - document.querySelector('header').clientHeight - this.usernameLabel.view.clientHeight - document.querySelector('.game__info').clientHeight - bottom - 20) / minCellSize) * 0.9) - 2; //2 - стены, 0.9 - запас по высоте с учетом панелей браузера и кнопок смартфона 
		if (this.game.maxRowsCount < this.game.minRowsCount) {
			this.game.maxRowsCount = this.game.minRowsCount;
		}
		this.game.maxColumnsCount = Math.floor((window.screen.availWidth - 50 - 50) / minCellSize) - 2;
		if (this.game.maxColumnsCount < this.game.minColumnsCount) {
			this.game.maxColumnsCount = this.game.minColumnsCount;
		}
        this.setCellSize();
	}

    processKey(key) {
        const changeDirection = (direction) => {
            this.game.nextDirection = direction;
            vibrateOnClick();
        }

        if (this.game.isLevelResetting || key === 'F11') {
            return;
        }
        switch (key) {
            case 'ArrowLeft':
                if (this.game.currentDirection === 'right') {
                    return;
                }
                changeDirection('left');
                break;
            case 'ArrowRight':
                if (this.game.currentDirection === 'left') {
                    return;
                }
                changeDirection('right');
                break;
            case 'ArrowUp':
                if (this.game.currentDirection === 'down') {
                    return;
                }
                changeDirection('up');
                break;
            case 'ArrowDown':
                if (this.game.currentDirection === 'up') {
                    return;
                }
                changeDirection('down');
                break;
            case 'Space':
                this.gameToggle();
                break;
        }
    }

    keyPressed(event) {
        if (this.dialogIsOpen()) {
            return;
        }
        this.processKey(event.code);
        event.preventDefault();
    }

    dialogIsOpen() {
        return !!document.querySelector('.dialog');
    }
    
    fieldClick(event) {
        const clickX = event.pageX;
        const clickY = event.pageY;
        let {x, y} = this.game.snake.headCoordinates();
        let key;
        switch (this.game.currentDirection) {
            case 'up':
            case 'down':
                key = clickX >= x ? 'ArrowRight' : 'ArrowLeft';
                break;
            case 'left':
            case 'right':
                key = clickY >= y ? 'ArrowDown' : 'ArrowUp';
                break;
        }
        this.processKey(key);
    }

    onGameStop() {
        this.inputController.setButtonsStates('stopped');
    }

    onGameStart() {
        this.inputController.setButtonsStates('running');
    }

    gameToggle() {
        const gameState = this.game.gameToggle();
        this.inputController.setButtonsStates(gameState);
    }

    showAboutDialog() {
        const dialog = new AboutDialog();
        return dialog.show();
    }
    
    async showConfigDialog() {
        const settings = {
            username: this.usernameLabel.text,
            columns: this.game.field.columnsCount,
            rows: this.game.field.rowsCount,
            showKeypad: this.inputController.keypad.isVisible(),
            showKeypadConfig: isTouchDevice(),
            minColumns: this.game.minColumnsCount,
            maxColumns: this.game.maxColumnsCount,
            minRows: this.game.minRowsCount,
            maxRows: this.game.maxRowsCount,
        };
        const configDialog = new ConfigDialog(settings);
        const newSettings = await configDialog.show();
        if (!newSettings) {
            return;
        }
        const newColumnsCount = +newSettings.columns;
        const newRowsCount = +newSettings.rows;
        this.usernameLabel.text  = newSettings.username;
        if (isTouchDevice()) {
            if (newSettings.showKeypad) {
                this.inputController.showKeypad();
                this.refreshControlButtons();
            } else {
                this.inputController.hideKeypad();
            }
        }
        if (this.game.field.columnsCount !== newColumnsCount || this.game.field.rowsCount !== newRowsCount) {
            this.game.configureAndReset(newRowsCount, newColumnsCount);
            this.setCellSize();
        }
        storage.saveSettings({
            rows: newRowsCount,
            columns: newColumnsCount,
            username: newSettings.username,
            keypad: newSettings.showKeypad,
        });
    }

    loadSettings() {
        const settings = storage.loadSettings();
        const newSettings = {};
        let rows = this.game.maxRowsCount;
        let columns = this.game.maxColumnsCount;
        let keypadVisible = false;
        if (settings.rows) {
            if (settings.rows <= this.game.maxRowsCount) {
                rows = settings.rows;
            }
        } else {
            rows = Math.floor((this.game.maxRowsCount + this.game.minRowsCount) / 2);
            newSettings.rows = rows;
        }
        if (settings.columns) {
            if (settings.columns <= this.game.maxColumnsCount) {
                columns = settings.columns;
            }
        } else {
            columns = Math.floor((this.game.maxColumnsCount + this.game.minColumnsCount) / 2);
            newSettings.columns = columns;
        }
        if (isTouchDevice()) {
            if (settings.keypad !== undefined) {
                keypadVisible = settings.keypad;
            } else {
                if (isTouchDevice()) {
                    keypadVisible = true;
                }
            }
            if (keypadVisible) {
                this.inputController.showKeypad();
            }
        }
        if (settings.username) {
            this.usernameLabel.text = settings.username;
        }
        storage.saveSettings(newSettings);
        this.game.configureAndReset(rows, columns);
    }

    setCellSize() {
        let gameContentWidth = this.view.clientWidth;
        const fieldContentWidth = this.game.areaView.clientWidth;
        if (this.inputController.controls.areButtonsRound()) {
            this.inputController.controls.setButtonsSquare();
            gameContentWidth = this.view.clientWidth;
            this.inputController.controls.setButtonsRound();
        }
        const wallWidth = defaultCellSize;
        const sizeStr = `clamp(${minCellSize}px, ${defaultCellSize}px, ${Math.floor((defaultCellSize / (gameContentWidth - fieldContentWidth + this.game.field.columnsCount * defaultCellSize + wallWidth * 2) * 100))}vw - 3px)`;
        this.game.cellSize = sizeStr;
        this.onResize();
    }

    onResize() {
        if (isTouchDevice() && !this.inputController.keypad.isVisible()) {
            return;
        }
        this.refreshControlButtons();
    }

    refreshControlButtons() {
        const cellSize = this.game.cellSize;
        if (cellSize) {
            cellSize < defaultCellSize ? this.inputController.controls.setButtonsRound() : this.inputController.controls.setButtonsSquare();
        }
    }
}
