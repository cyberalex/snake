import MainControls from "../components/controls";
import KeyPad from "../components/keypad";

export default class InputController {
    controls;
    keypad;

    constructor(
        newGameCallback, 
        gameStartStopCallback, 
        showConfigDialogCallback, 
        showAboutDialogCallback, 
        processKeyCallback
    ) {
        this.controls = new MainControls(newGameCallback, gameStartStopCallback, showConfigDialogCallback, showAboutDialogCallback);
		this.keypad = new KeyPad(processKeyCallback, gameStartStopCallback);
    }

    showKeypad() {
        this.controls.startButton.hide();
        this.controls.setColumnLayout();
        this.controls.setPositionLeft();
        this.keypad.setState('stopped');
        this.keypad.show();
    }

    hideKeypad() {
        this.controls.startButton.show();
        this.controls.setRowLayout();
        this.controls.setPositionBottom();
        this.controls.setButtonsRound();
        this.keypad.hide();
    }

    setButtonsStates(gameState) {
        this.controls.setState(gameState);
        this.keypad.setState(gameState);
    }
}