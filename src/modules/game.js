import { Snake } from "../components/game-area/snake";
import Targets from "../components/game-area/target";
import Field from "../ui/game-area/field";
import Wall from "../ui/game-area/wall";
import LevelValueLabel from "../components/level-value-label";
import ScoreValueLabel from "../components/score-value-label";
import Stopwatch from "../components/stopwatch";
import { WinDialog, FailDialog } from "../components/dialogs/game-over-dialog";
import { minMaxSnakeLength, minSnakeLength, resetSpeedNewGame, resetSpeedNextLevel, startTimerInterval, winTimerInterval } from "../shared/settings";

export default class Game {
    field;
	snake;
	targets;
    scoreLabel;
    levelLabel;
    stopwatch;
	areaView;
	state = 'stopped';
	isLevelResetting = false;
	level = 1;
	score = 0;
	currentDirection = 'up';
	nextDirection = 'up';
	timer;
	timerInterval = startTimerInterval; 
	maxSnakeLength = 10;
	maxRowsCount = 20;
	maxColumnsCount = 40;
	minRowsCount = 10;
	minColumnsCount = 10;
	maxTargets = 1;

	constructor() {
        this.scoreLabel = new ScoreValueLabel();
        this.levelLabel = new LevelValueLabel();
        this.stopwatch = new Stopwatch();
		this.field = new Field();
		this.wall = new Wall();
		this.snake = new Snake();
		this.targets = new Targets();
		this.areaView = document.querySelector('.game__area');
	}

	set cellSize(value) {
		this.field.cellSize = value;
        this.wall.baseSize = value;
	}

	get cellSize() {
		return this.field.cellSize;
	}
	    
    updateScoreboard() {
        this.scoreLabel.text = this.score;
        this.levelLabel.text = this.level;
    }

	async newLevel(levelFunction, level) {
		this.isLevelResetting = true;
		this.stopTimer();
		let resetSpeed = resetSpeedNextLevel;
		const targetImageIndex = this.targets.lastTargetImageIndex;
		if (!level) {
			level = 1;
			resetSpeed = resetSpeedNewGame;
		}
		await this.field.resetDownAnimation(resetSpeed, targetImageIndex);
		levelFunction.call(this, level);
		await this.field.resetUpAnimation(resetSpeed);
		if (level > 1) {
			this.startTimer();
		}
		this.isLevelResetting = false;
	}

	configureAndReset(rowsCount, columnsCount) {
		this.makeField(rowsCount, columnsCount);
		this.setMaxSnakeAndTargets();
		this.gameInit();
	}
	
	gameInit() {
		this.stopwatch.reset();
		this.score = 0;
		this.makeLevel(1);
	}
	
	newGame() {
		if (this.isLevelResetting) {
			return;
		}
		this.gameStop();
		this.gameInit();
	}
	
	calcTimerInterval(level) {
		return startTimerInterval - (50 * (level - 1) - level * 3);
	}
	
	makeField(rowsCount, columnsCount) {
		this.wall.init(rowsCount, columnsCount, 0);
		this.field.init(rowsCount, columnsCount, 0);
	}

	makeLevel(level) {
		let x, y;

		this.level = level;
		this.currentDirection = 'up';
		this.nextDirection = 'up';
		this.timerInterval = this.calcTimerInterval(level);
		if (this.timerInterval <= winTimerInterval) {
			this.newGame();
			return;
		}
		this.field.clear();
		x = Math.round(this.field.columnsCount / 2) - 1;
		y = this.field.rowsCount - 1;
		this.snake.reset(x, y);
		this.targets.generate(this.maxTargets, this.field.rowsCount, this.field.columnsCount, this.snake.coordinates);
		this.updateScoreboard();
	}

	move() {
		let dx = 0, dy = 0;
		let gameOver = false;
		let cought;
		switch (this.nextDirection) {
			case 'up':
				this.snake.head.y > 0 ? dy = -1 : gameOver = true;
				break;
			case 'down':
				this.snake.head.y < this.field.rowsCount - 1 ? dy = 1 : gameOver = true;
				break;
			case 'left':
				this.snake.head.x > 0 ? dx = -1 : gameOver = true;
				break;
			case 'right':
				this.snake.head.x < this.field.columnsCount - 1 ? dx = 1 : gameOver = true;
				break;
		}
		cought = this.targets.contain(this.snake.head.x + dx, this.snake.head.y + dy);
		if (!gameOver) {
			if (!this.snake.move(dx, dy, cought)) {
				gameOver = true;
			}
		}
	
		if (gameOver) {
			this.gameStop();
			this.snake.setFailed();
			this.stopwatch.pause();
			this.showFailDialog().then(() => {
				this.newLevel(this.gameInit.bind(this));
			});
			return;
		}
		this.currentDirection = this.nextDirection;
		if (cought) {
			this.targetCought();
		}
	}
	
	setMaxSnakeAndTargets() {
		let max = Math.floor(this.field.rowsCount * this.field.columnsCount / 100);
		this.maxTargets = max > 1 ? max : 1;
		max = this.maxTargets * 2 + minSnakeLength;
		this.maxSnakeLength = max >= minMaxSnakeLength ? max : this.maxTargets * Math.ceil((minMaxSnakeLength - minSnakeLength) / this.maxTargets) + minSnakeLength;
	}
	
	targetCought() {
		this.score += this.level;
		if (this.snake.length === this.maxSnakeLength)	{
			if (this.calcTimerInterval(this.level + 1) <= winTimerInterval) {
				this.gameStop();
				this.stopwatch.pause();
				this.updateScoreboard();
				this.showWinDialog().then(() => {
					this.newLevel(this.gameInit.bind(this));
				});
				this.level = 1;
				return;
			} else {
				this.newLevel(this.makeLevel, this.level + 1);
			}
		}
		else {
			this.targets.remove(this.snake.head.x, this.snake.head.y);
			if (this.targets.count === 0) {
				this.targets.generate(this.maxTargets, this.field.rowsCount, this.field.columnsCount, this.snake.coordinates);
			}
		}
		this.updateScoreboard();
	}

	startTimer() {
		this.stopTimer();
		this.timer = setInterval(this.move.bind(this), this.timerInterval);
	}
	
	stopTimer() {
		this.timer = clearInterval(this.timer);
	}
	
	gameToggle() {
		if (this.isLevelResetting) {
			return 'resetting';
		}
		this.state === 'stopped' ? this.gameStart() : this.gameStop();
		return this.state;
	}

	gameStart() {
		this.startTimer();
		this.stopwatch.start();
		this.state = 'running';
		dispatchEvent(new CustomEvent('gameStart'));
	}
	
	gameStop() {
		this.stopTimer();
		this.stopwatch.pause();
		this.state = 'stopped';
  		dispatchEvent(new CustomEvent('gameStop'));
	}

	showWinDialog() {
        const dialog = new WinDialog(this.score, this.level, this.stopwatch.text);
        return dialog.show();
    }
    
    showFailDialog() {
        const dialog = new FailDialog(this.score, this.level, this.stopwatch.text);
        return dialog.show();
    }
}
