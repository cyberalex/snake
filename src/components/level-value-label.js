import { TextLabel } from "../ui/text-label";

export default class LevelValueLabel extends TextLabel {
    constructor() {
        super('.game__info-value_level');
    }
}