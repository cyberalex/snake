import { TextLabel } from "../ui/text-label";

export default class ScoreValueLabel extends TextLabel {
    constructor() {
        super('.game__info-value_score');
    }
}