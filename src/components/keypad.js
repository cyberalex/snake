import { Button, StartButton } from "../ui/button";
import Control from "../ui/control";

export default class KeyPad extends Control {
    startButton;
    arrowButtons = [];

    constructor(keyCallback, startButtonCallback) {
        super('.keypad');
        this.startButton = new StartButton('#start-button-mobile');
        this.startButton.setOnClick(startButtonCallback);
        this.view.querySelectorAll('.keypad__key').forEach((key) => this.arrowButtons.push(new Button(key)));
        this.arrowButtons.forEach((key) => key.setOnClick((event) => keyCallback(event.target.value)));
    }

    disableArrowButtons() {
        this.arrowButtons.forEach((button) => button.disable());
    }

    enableArrowButtons() {
        this.arrowButtons.forEach((button) => button.enable());
    }

    setState(gameState) {
        if (gameState === 'running') {
            this.enableArrowButtons();
            this.startButton.setStatePause();
        } else {
            this.disableArrowButtons();
            this.startButton.setStatePlay();
        }
    }
}
