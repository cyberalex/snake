export class Snake {
    body = [];
    currentDirection;
    nextGrow;

    reset(tailX, tailY) {
        this.body.forEach((cell) => cell.remove());
        this.body = [];
        this.body.push(new SnakeHead(tailX, tailY - 2));
        this.body.push(new SnakeMiddle(tailX, tailY - 1));
        this.body.push(new SnakeTail(tailX, tailY));
        this.currentDirection = 'up';
        this.nextGrow = false;
    }

    headCoordinates() {
        const snakeHeadRect = this.head.view.getBoundingClientRect();
        const cellSize = this.head.size;
        const x = snakeHeadRect.left + cellSize / 2;
        const y = snakeHeadRect.top + cellSize / 2;
        return {x, y};
    }

    get y() {

    }

    get coordinates() {
        return this.body.map((cell) => ({x: cell.x, y: cell.y}));
    }

    get headIndex() {
        return 0;
    }

    get tailIndex() {
        return this.body.length - 1;
    }

    get head() {
        return this.body[this.headIndex];
    }

    get tail() {
        return this.body[this.tailIndex];
    }

    get length() {
        return this.body.length;
    }

    move(dx, dy, grow) {
        if (this.body.slice(2, this.tailIndex).some((cell) => cell.x === this.head.x + dx && cell.y === this.head.y + dy)) {
            return false;
        }
        
        let direction;
        if (!this.nextGrow) {
            this.tail.remove();
            this.body.pop();
            this.body[this.body.length - 1].remove();
            let tempCell = this.body.pop();
            if (this.body[this.body.length - 1].y === tempCell.y) {
                direction = this.body[this.body.length - 1].x > tempCell.x ? 'left' : 'right';
            } else {
                direction = this.body[this.body.length - 1].y > tempCell.y ? 'down' : 'up';
            }
            this.body.push(new SnakeTail(tempCell.x, tempCell.y, direction));
        } else {
            this.nextGrow = false;
        }
        this.nextGrow = grow;
        
		if (dy === 0) {
            direction = dx > 0 ? 'right' : 'left';
		} else {
            direction = dy > 0 ? 'down' : 'up';
		}

        this.head.remove();
        this.body.unshift(new SnakeHead(this.head.x + dx, this.head.y + dy, direction));
        this.body[1] = new SnakeMiddle(this.body[1].x, this.body[1].y, this.currentDirection, direction);
        this.currentDirection = direction;
        return true;
    }

    setFailed() {
        this.head.setFailed();
    }

    cellView(index) {
        return body[index].view;
    }
}

class SnakeCell {
    x;
    y;
    view;
    img;
    
    constructor(x, y, imageSrc) {
        this.view = document.querySelector('.game__field').children[y].children[x];
        this.view.classList.add('game__cell_snake');
        const img = document.createElement('img');
        img.src = imageSrc;
        this.img = img;
        this.view.append(img);
        this.x = x;
        this.y = y;
    }

    remove() {
        this.view.removeAttribute('class');
        this.view.classList.add('game__cell');
		this.img.remove();
    }

    get size() {
        return parseInt(getComputedStyle(this.view).height);
    }
}

class SnakeHead extends SnakeCell {
    constructor(x, y, direction = 'up') {
        super(x, y, 'img/snake_head.png');
        this.img.classList.add('snake__head', `snake__head_${direction}`);
    }

    setFailed() {
        this.view.classList.add('game__cell_fail');
		this.img.src = 'img/snake_head_fail.png';
    }
}

class SnakeMiddle extends SnakeCell {
    constructor(x, y, currentDirection = 'up', nextDirection = 'up') {
        let orientation;
        let type;
        if (currentDirection === nextDirection) {
            type = 'straight';
            orientation = (currentDirection == 'up' || currentDirection == 'down') ? 'vertical' : 'horizontal';
        } else {
            type = 'turn';
            orientation = `${currentDirection}-${nextDirection}`;
        }
        super(x, y, `img/snake_body_${type}.png`);
        this.img.classList.add(`snake__body-${type}`, `snake__body-${type}_${orientation}`);
    }
}

class SnakeTail extends SnakeCell {
    constructor(x, y, direction = 'up') {
        super(x, y, 'img/snake_tail.png');
        this.img.classList.add('snake__tail', `snake__tail_${direction}`);
    }
}