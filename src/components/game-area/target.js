import { getRandomInt } from "../../shared/shared";
import { targetFilesCount } from "../../shared/settings";

export default class Targets {
    list = [];

    add(target) {
        this.list.push(target);
    }

    contain(x, y) {
        return this.list.some((target) => target.x === x && target.y === y);
    }

    remove(x, y) {
        const targetIndex = this.list.findIndex((target) => target.x === x && target.y === y);
        this.list[targetIndex].remove();
        this.list.splice(targetIndex, 1);
    }

    clear() {
        this.list = [];
    }

    get count() {
        return this.list.length;
    }

    get lastTargetImageIndex() {
        return this.list[0].imgIndex;
    }

    generate(targetsCount, rowsCount, columnsCount, exclude) {
        this.clear();
        let x, y;
        for (let i = 0; i < targetsCount; i += 1) {
            let targetReady = false;
            do {
                x = getRandomInt(0, columnsCount - 1);
                y = getRandomInt(0, rowsCount - 1);
                if (exclude.some((item) => item.x === x && item.y === y) || this.contain(x, y)) {
                    continue;
                }
                targetReady = true;
            } while (!targetReady);
            this.add(new Target(x, y));
        }
    }
}

class Target {
    view;
    img;
    imgIndex;
    x;
    y;

    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.imgIndex = getRandomInt(1, targetFilesCount);
        this.img = document.createElement('img');
        this.img.classList.add('game__cell_target-img');
        this.img.src = `img/target${this.imgIndex}.png`;
        this.view = document.querySelector('.game__field').children[y].children[x];
        this.view.classList.add('game__cell_target');
        this.view.append(this.img);
        this.img.onload = () => {
            setTimeout(() => this.img.classList.add('scale1'), 20);
        }
    }

    remove() {
        this.view.classList.remove('game__cell_target');
        this.img.remove();
    }
}
