import { Button, StartButton } from "../ui/button";
import Control from "../ui/control";

export default class MainControls extends Control {
    startButton;
    newGameButton;
    configButton;
    aboutButton;
    buttons = [];

    constructor(newGameButtonCallback, startButtonCallback, configButtonCallback, aboutButtonCallback) {
        super('.buttons');
        this.startButton = new StartButton('#start-button');
        this.startButton.setOnClick(startButtonCallback);
        this.newGameButton = new Button('#new-game-button');
        this.newGameButton.setOnClick((event) => {
            event.currentTarget.blur();
            newGameButtonCallback();
        });
        this.configButton = new Button('#config-button');
        this.configButton.setOnClick(configButtonCallback);
        this.aboutButton = new Button('#about-button');
        this.aboutButton.setOnClick(aboutButtonCallback);
        this.buttons = [this.newGameButton, this.startButton, this.configButton, this.aboutButton];
    }

    setState(gameState) {
        if (gameState === 'running' || gameState === 'resetting') {
            this.configButton.disable();
            this.aboutButton.disable();
            this.startButton.setStatePause();
        } else {
            this.configButton.enable();
            this.aboutButton.enable();
            this.startButton.setStatePlay();
        }
    }

    areButtonsRound() {
        return this.startButton.isRound;
    }

    setButtonsRound() {
        this.buttons.forEach((button) => button.setRound());
    }

    setButtonsSquare() {
        this.buttons.forEach((button) => button.setSquare());
    }

    setRowLayout() {
		this.view.querySelector('.buttons__content').classList.add('buttons__content_row');
    }

    setColumnLayout() {
		this.view.querySelector('.buttons__content').classList.remove('buttons__content_row');
    }

    setPositionLeft() {
        document.querySelector('.game__content').classList.remove('game__content_buttons-under');
    }

    setPositionBottom() {
        document.querySelector('.game__content').classList.add('game__content_buttons-under');
    }
}
