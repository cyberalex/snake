import { TextLabel } from "../ui/text-label";

export default class Stopwatch extends TextLabel {
    value;
    timer;

    constructor() {
        super('.game__stopwatch');
        this.value = 0;
        this.render();
    }

    reset() {
        this.timer = clearInterval(this.timer);
        this.value = 0;
        this.render();
    }

    pause() {
        this.timer = clearInterval(this.timer);
    }

    start() {
        this.timer = setInterval(() => {
            ++this.value;
            this.render();
        }, 10);
    }

    render() {
        const min = Math.floor(this.value / 60 / 100);
        const sec = Math.floor(this.value / 100) - min * 60;
        const minStr = min < 10 ? '0' + min.toString() : min.toString();
        const secStr = sec < 10 ? '0' + sec.toString() : sec.toString();
        const newTextValue = `${minStr}:${secStr}`;
        if (this.text !== newTextValue) {
            this.text = newTextValue;
        }
    }
}
