import { appVersion, appDate } from "../../shared/app-info";
import { InfoDialog } from "../../ui/dialog";

export default class AboutDialog extends InfoDialog {
    constructor() {
        super('about-dialog');
        this.view.querySelector('.app-version').textContent = appVersion;
        this.view.querySelector('.app-date').textContent = appDate;
    }
}