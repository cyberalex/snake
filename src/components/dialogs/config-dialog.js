import { Button } from "../../ui/button";
import CheckBox from "../../ui/checkbox";
import { Dialog } from "../../ui/dialog";
import Input from "../../ui/input";
import Range from "../../ui/range";

export default class ConfigDialog extends Dialog {
    username;
    columns;
    rows;
    showKeypad;
    okButton;
    cancelButton;

    constructor(settings) {
        super('config-dialog');
        this.username = new Input('#config-username', settings.username);
        this.columns = new Range('#config-columns', settings.columns, settings.minColumns, settings.maxColumns);
        this.rows = new Range('#config-rows', settings.rows, settings.minRows, settings.maxRows);
        if (settings.showKeypadConfig) {
            this.showKeypad = new CheckBox('#config-show-keypad', settings.showKeypad);
        }
        this.okButton = new Button(this.view.querySelector('#config-dialog-ok-button'));
        this.cancelButton = new Button(this.view.querySelector('#config-dialog-cancel-button'));
    }

    show() {
        this.showView();
        this.okButton.view.focus();
        return new Promise((resolve) => {
            const dialogKeyPress = (event) => {
                switch (event.code) {
                    case 'Escape': 
                        this.close(resolve, false);
                        break;
                    case 'Enter':
                    case 'NumpadEnter':
                        this.close(resolve, true);
                        break;
                }
            }

            document.addEventListener('keydown', dialogKeyPress);
            this.okButton.setOnClick(() => this.close(resolve, true));
            this.cancelButton.setOnClick(() => this.close(resolve, false));
        });
    }

    close(resolveFunc, accepted) {
        if (!accepted) {
            super.close(resolveFunc);
        }

        const settings = {
            username: this.username.value,
            columns: this.columns.value, 
            rows: this.rows.value,
            showKeypad: !!this.showKeypad?.checked,
        };
        super.close(resolveFunc, settings);
    }
}