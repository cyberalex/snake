import Control from "../../ui/control";
import { InfoDialog } from "../../ui/dialog";

export class GameOverDialog extends InfoDialog {
    constructor(dialogType, score, level, time) {
        super('gameover-dialog');
        this.view.querySelector('#gameover-dialog-score').innerHTML = score;
        this.view.querySelector('#gameover-dialog-levels').innerHTML = level;
        this.view.querySelector('#gameover-dialog-time').innerHTML = time;
        const captionWin = new Control('.dialog__caption_win');
        const captionFail = new Control('.dialog__caption_fail');
        if (dialogType === 'win') {
            captionWin.show();
            captionFail.hide();
        } else {
            captionWin.hide();
            captionFail.show();
        }
    }
}

export class WinDialog extends GameOverDialog {
    constructor(score, level, time) {
        super('win', score, level, time);
    }
}

export class FailDialog extends GameOverDialog {
    constructor(score, level, time) {
        super('fail', score, level, time);
    }
}