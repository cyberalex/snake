import { TextLabel } from "../ui/text-label";

export default class UsernameLabel extends TextLabel {
    constructor() {
        super('.game__username');
    }
}