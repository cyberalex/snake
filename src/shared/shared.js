export function getRandomInt(min, max) {
	return Math.floor(Math.random() * ((max + 1) - min)) + min;
}

export function isTouchDevice() {
    return (window.matchMedia('(hover: none)').matches || 
        ('ontouchstart' in window) ||
        (navigator.maxTouchPoints > 0) ||
        (navigator.msMaxTouchPoints > 0));
}
  
export function vibrateOnClick() {
    window.navigator.vibrate(50);
}