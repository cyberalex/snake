export const startTimerInterval = 500;
export const targetFilesCount = 3;
export const resetSpeedNextLevel = 20;
export const resetSpeedNewGame = 100;
export const winTimerInterval = 50;
export const defaultCellSize = 30;
export const minCellSize = 20;
export const minMaxSnakeLength = 10;
export const minSnakeLength = 3;