# Snake Game
[👉 Deployment](https://cyberalex.gitlab.io/snake/)  
##### Description:  
- Module structure
- ES6 OOP
- Saving settings to the local storage
- Adaptive and responsive
- Additional arrow keypad for touchscreens
- Game field size is configurable
- Amount of targets depends on the field size
- User can use arrow buttons for snake movement direction or click on the field
- Space button for play/stop
  
`HTML` `SCSS` `JavaScript` `Webpack`